Orca Screeps AI
===============

This is my screeps bot written with Kotlin.


Install
-------

Run `gradle build` to install all dependencies and build the bot. 
All files are going to end up in the `./bot/build/kotlin-js-min/main` folder.
