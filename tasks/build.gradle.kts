import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile

plugins {
    id("kotlin2js") version "1.3.0-rc-146"
    id("kotlinx-serialization") version "1.3.0-rc-146"
}

group = "de.fkrauthan.screeps.orca"
version = "1.0-SNAPSHOT"


dependencies {
    compile(kotlin("stdlib-js"))
    compile("org.jetbrains.kotlinx:kotlinx-serialization-runtime-js:0.8.1-rc13")

    implementation(project(":common"))
    implementation(project(":navigation"))
}

tasks {
    "compileKotlin2Js"(Kotlin2JsCompile::class) {
        kotlinOptions {
            moduleKind = "commonjs"
            sourceMap = true
            metaInfo = true
        }
    }
}
