package de.fkrauthan.screeps.orca.tasks.impl

import de.fkrauthan.screeps.orca.tasks.TargetType
import de.fkrauthan.screeps.orca.tasks.Task
import de.fkrauthan.screeps.orca.tasks.TaskOptions
import screeps.api.ConstructionSite
import screeps.api.ScreepsReturnCode

class BuildTask(target: ConstructionSite, options: TaskOptions = TaskOptions()) : Task(TargetType(ref = target.id, pos = target.pos), options) {

    companion object {
        var TASK_NAME = "build"

        fun create(target: ConstructionSite, options: TaskOptions = TaskOptions()): BuildTask = BuildTask(target, options)
    }

    init {
        this.settings.targetRange = 3
        this.settings.workOffRoad = true
    }

    override fun name(): String = TASK_NAME

    override fun isValidTask(): Boolean = this.creep.carry.energy > 0
    override fun isValidTarget(): Boolean {
        val target = this.target as ConstructionSite?
        return target != null && target.my && target.progress < target.progressTotal
    }

    override fun work(): ScreepsReturnCode {
        val target = this.target as ConstructionSite
        // TODO add move off logic
//        if (target != null && !target.isWalkable) {
//            val creepOnTarget = target.pos.lookFor<Creep>(LOOK_CREEPS)?.get(0);
//            if (creepOnTarget != null) {
//                const zerg = Overmind.zerg[creepOnTarget.name];
//                if (zerg) {
//                    this.creep.say('move pls');
//                    zerg.moveOffCurrentPos();
//                }
//            }
//        }
        return this.creep.build(target);
    }

}
