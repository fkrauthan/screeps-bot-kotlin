package de.fkrauthan.screeps.orca.tasks.impl

import de.fkrauthan.screeps.orca.tasks.TargetType
import de.fkrauthan.screeps.orca.tasks.Task
import de.fkrauthan.screeps.orca.tasks.TaskOptions
import screeps.api.HasPosition
import screeps.api.OK
import screeps.api.RoomPosition
import screeps.api.ScreepsReturnCode

class GoToTask (target: RoomPosition, options: TaskOptions = TaskOptions()) : Task(TargetType(ref = "", pos = target), options) {

    companion object {
        var TASK_NAME = "goTo"

        fun create(target: RoomPosition, options: TaskOptions = TaskOptions()): GoToTask = GoToTask(target, options)
        fun create(target: HasPosition, options: TaskOptions = TaskOptions()): GoToTask = GoToTask(target.pos, options)
    }

    init {
        this.settings.targetRange = 1
    }

    override fun name(): String = TASK_NAME

    override fun isValidTask(): Boolean = !this.creep.pos.inRangeTo(this.targetPos, this.settings.targetRange)
    override fun isValidTarget(): Boolean = true

    override fun work(): ScreepsReturnCode = OK

}
