package de.fkrauthan.screeps.orca.tasks

import de.fkrauthan.screeps.orca.common.Helpers
import de.fkrauthan.screeps.orca.common.RoomPositionExtension.isEdge
import de.fkrauthan.screeps.orca.tasks.CreepExtension.task
import kotlinx.serialization.Serializable
import screeps.api.*
import screeps.api.Game
import screeps.api.RoomPosition

class TaskSettings(
        var targetRange: Int = 1,
        var workOffRoad: Boolean = false,
        var oneShot: Boolean = false,
        var timeout: Int = Int.MAX_VALUE,
        var blind: Boolean = true
)

@Serializable
data class TaskOptions(
        var blind: Boolean = true,
        var nextPos: RoomPosition? = null
)

@Serializable
data class TargetType(
        var ref: String = "",
        var pos: RoomPosition
)

@Serializable
data class TaskData(
        var quiet: Boolean? = null,
        var resourceType: ResourceConstant? = null,
        var amount: Int? = null
)

@Serializable
data class TaskProto(
        var name: String,
        var creep: String,
        var target: TargetType,
        var options: TaskOptions,
        var data: TaskData
)

abstract class Task(var _target: TargetType, var options: TaskOptions = TaskOptions()) {

    protected var settings: TaskSettings = TaskSettings()

    protected var _creep: String = ""

    protected var _parent: TaskProto? = null

    protected var data: TaskData = TaskData()

    protected var tick: Int = Game.time
        get() = Game.time

    protected var parent: Task? = null
        get() {
            if (field != null) {
                return field
            }
            if (_parent != null) {
                field = Tasks.create(_parent!!)
                return field
            }
            return null
        }
        set(value) {
            field = value
            _parent = value?.proto
        }

    var creep: Creep
        get() = Game.creeps[_creep]!!
        set(creep) {
            _creep = creep.name
        }

    protected val target: RoomObject?
        get() {
            return Helpers.deref(_target.ref)
        }

    protected val targetPos: RoomPosition
        get() {
            return _target.pos
        }

    protected val isWorking: Boolean
        get() = this.creep.pos.inRangeTo(this.targetPos, this.settings.targetRange) && !this.creep.pos.isEdge

    protected val proto: TaskProto
        get() = TaskProto(name(), _creep, _target, options, data)

    abstract fun name(): String

    abstract fun isValidTask(): Boolean
    abstract fun isValidTarget(): Boolean

    abstract fun work(): ScreepsReturnCode

    fun fork(newTask: Task): Task {
        newTask.parent = this;
        if (this._creep.isNotEmpty()) {
            this.creep.task = newTask;
        }
        return newTask;
    }

    fun moveToNextPos(): ScreepsReturnCode? {
//        if (this.options.nextPos != null && this._creep.isNotEmpty()) {
//            val nextPos = Helpers.derefRoomPosition(this.options.nextPos!!);
//            return this.creep.navigateTo(nextPos)
//        }
        return null
    }

    fun moveToTarget(range: Int = this.settings.targetRange): ScreepsReturnCode {
//        if (this.options.moveOptions != null && this.options.moveOptions!!.range == 0) {
//            this.options.moveOptions!!.range = range;
//        }
//        return this.creep.navigateTo(this.targetPos, this.options.moveOptions)
        return OK
    }

    fun isValid(): Boolean {
        var validTask = false;
        if (this._creep.isNotEmpty()) {
            validTask = this.isValidTask() && Game.time - this.tick < this.settings.timeout;
        }
        var validTarget = false;
        if (this.target != null) {
            validTarget = this.isValidTarget();
        } else if ((this.settings.blind || this.options.blind) && Game.rooms[this.targetPos.roomName] != null) {
            // If you can't see the target's room but you have blind enabled, then that's okay
            validTarget = true;
        }
        // Return if the task is valid; if not, finalize/delete the task and return false
        if (validTask && validTarget) {
            return true;
        } else {
            // Switch to parent task if there is one
            this.finish();
            return this.parent?.isValid() ?: false
        }
    }

    fun run(): ScreepsReturnCode? {
        if (this.isWorking) {
            if (this.settings.workOffRoad) {
                // Move to somewhere nearby that isn't on a road
//                TODO: this.parkCreep(this.creep, this.targetPos, true)
            }
            val result = this.work()
            if (this.settings.oneShot && result === OK) {
                this.finish()
            }
            return result
        } else {
            this.moveToTarget();
            return null
        }
    }

    fun finish() {
        this.moveToNextPos();
        if (this._creep.isNotEmpty()) {
            this.creep.task = this.parent
        } else {
//            console.log("No creep executing ${this.name}! Proto: ${JSON.stringify(this.proto)}");
        }
    }

//    override var data: TaskData = TaskData()
//    protected var parent: Task? = null
//    private var _targetPos: RoomPosition? = null
//
//    init {
//        if (target != null) {
//            _target = TargetRef(ref = target.ref, pos = target.pos.unsafeCast<ProtoPos>())
//        }
//    }
//
//    protected val target: RoomObject?
//        get() {
//            return Helpers.deref(_target.ref)
//        }
//
//    protected val targetPos: RoomPosition
//        get() {
//            if (_targetPos == null) {
//                val derefTarget = target
//                if (derefTarget != null) {
//                    this._target.pos = derefTarget.pos.unsafeCast<ProtoPos>()
//                }
//                this._targetPos = Helpers.derefRoomPosition(this._target.pos);
//            }
//            return _targetPos!!
//        }
//

//

//

//
//    /* Moves to the next position on the agenda if specified - call this in some tasks after work() is completed */

//
//
//    // Finalize the task and switch to parent task (or null if there is none)


}
