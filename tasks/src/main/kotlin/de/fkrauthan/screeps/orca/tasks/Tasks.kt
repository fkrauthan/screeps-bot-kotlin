package de.fkrauthan.screeps.orca.tasks

import de.fkrauthan.screeps.orca.common.Helpers
import de.fkrauthan.screeps.orca.tasks.impl.BuildTask
import de.fkrauthan.screeps.orca.tasks.impl.GoToTask
import screeps.api.ConstructionSite

object Tasks {

    fun create(proto: TaskProto): Task? {
        val structure = Helpers.deref(proto.target.ref)

        if (structure != null) {
            return when (proto.name) {
                GoToTask.TASK_NAME -> GoToTask.create(structure, proto.options)
                BuildTask.TASK_NAME -> BuildTask.create(structure as ConstructionSite, proto.options)
                else -> null
            }
        }

        return when (proto.name) {
            GoToTask.TASK_NAME -> GoToTask.create(proto.target.pos, proto.options)
            else -> null
        }
    }

}
