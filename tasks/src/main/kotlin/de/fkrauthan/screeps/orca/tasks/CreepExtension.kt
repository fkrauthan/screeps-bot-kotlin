package de.fkrauthan.screeps.orca.tasks

import screeps.api.CreepMemory
import kotlinx.serialization.json.JSON
import screeps.api.Creep

object CreepExtension {

    var CreepMemory.task: Task?
        get() {
            val internal = this.asDynamic()._task
            return if (internal == null) null else Tasks.create(JSON.parse(internal as String))
        }
        set(value) {
            val stringyfied = if (value == null) null else JSON.stringify(value)
            this.asDynamic()._task = stringyfied
        }

    var Creep.task: Task?
        get() {
            var field = this.asDynamic()._task
            if (field == null) {
                field = this.memory.task
                this.asDynamic()._task = field
            }
            return field
        }
        set(value) {
            this.asDynamic()._task = value
            this.memory.task = value
        }

}
