import de.fkrauthan.screeps.orca.tasks.CreepExtension.task
import de.fkrauthan.screeps.orca.tasks.impl.GoToTask
import screeps.api.Game
import screeps.api.get
import screeps.api.keys

/**
 * Entry point
 * is called by screeps
 *
 * must not be removed by DCE
 */
@Suppress("unused")
fun loop() {
    console.log("Hello World")

    val flagKey = Game.flags.keys[0]
    val creepName = Game.creeps.keys[0]

    val goToTask = GoToTask.create(Game.flags[flagKey]!!)
    Game.creeps[creepName]!!.task = goToTask

//    TravelerCreep
}
