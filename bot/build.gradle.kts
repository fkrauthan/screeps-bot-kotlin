import org.apache.tools.ant.taskdefs.condition.Os
import org.jetbrains.kotlin.gradle.dsl.KotlinJsDce
import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile

plugins {
    id("kotlin2js") version "1.3.0-rc-146"
    id("kotlin-dce-js") version "1.3.0-rc-146"
}

group = "de.fkrauthan.screeps.orca"
version = "1.0-SNAPSHOT"

dependencies {
    compile(kotlin("stdlib-js"))

    compile(project(":common"))
    compile(project(":navigation"))
    compile(project(":tasks"))
}


val SCREEPS_SERVER = System.getenv("SCREEPS_SERVER") ?: "127.0.0.1:21025"
val SCREEPS_BRANCH = System.getenv("SCREEPS_BRANCH") ?: "default"

setProperty("archivesBaseName", "main")

tasks {
    "compileKotlin2Js"(Kotlin2JsCompile::class) {
        kotlinOptions {
            moduleKind = "commonjs"
            sourceMap = true
            metaInfo = true
        }
    }

    "runDceKotlinJs"(KotlinJsDce::class) {
        keep("main.loop")
        dceOptions.devMode = false
    }

    "collectResources"(Copy::class) {
        group = "build"
        description = "Collect all sub package javascript resources"

        from(configurations.runtimeClasspath
                .filter { it.name.endsWith(".jar") }
                .map { zipTree(it) }
        ) {
            include("lib/*.js")
        }
        into("$buildDir/externalResources")
    }

    "deploy"(Copy::class) {
        group = "build"
        description = "Deploy the bot to screeps Steam edition"

        dependsOn("build")
        dependsOn("collectResources")


        // Define destination
        var destination = ""
        if (Os.isFamily(Os.FAMILY_WINDOWS)) {
            destination = "${ant.properties["user.home"]}/AppData/Local/Screeps/scripts"
        } else if (Os.isFamily(Os.FAMILY_MAC)) {
            destination = "${ant.properties["user.home"]}/Library/Application Support/Screeps/scripts"
        } else {
            throw IllegalArgumentException("Current OS is not supported!")
        }

        val server = SCREEPS_SERVER
                .replace(".", "_")
                .replace(":", "___")
        destination = "$destination/$server/$SCREEPS_BRANCH"


        // Copy required files
        includeEmptyDirs = false

        from("$buildDir/kotlin-js-min/main") {
            include("*.js")
        }
        from("$buildDir/externalResources/lib")

        into(destination)
    }
}
