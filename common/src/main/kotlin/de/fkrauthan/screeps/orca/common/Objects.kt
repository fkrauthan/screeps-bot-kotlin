package de.fkrauthan.screeps.orca.common

data class ProtoPos (
    var x: Int = 0,
    var y: Int = 0,
    var roomName: String = ""
)
