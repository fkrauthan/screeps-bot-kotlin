package de.fkrauthan.screeps.orca.common

import screeps.api.*

object Helpers {
    fun deref(ref: String): RoomObject? {
        if (ref.isEmpty()) {
            return null
        }
        return Game.getObjectById<Identifiable>(ref)?.unsafeCast<RoomObject?>() ?: Game.flags[ref] ?: Game.creeps[ref] ?: Game.spawns[ref]
    }

    fun derefRoomPosition(protoPos: ProtoPos): RoomPosition {
        return RoomPosition(protoPos.x, protoPos.y, protoPos.roomName)
    }
}
