package de.fkrauthan.screeps.orca.common

import screeps.api.RoomPosition

object RoomPositionExtension {

    val RoomPosition.isEdge: Boolean
        get() = this.x == 0 || this.x == 49 || this.y == 0 || this.y == 49

}