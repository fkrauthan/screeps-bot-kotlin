package de.fkrauthan.screeps.orca.navigation

import screeps.api.Creep
import screeps.api.RoomPosition

object CreepExtension {

    fun Creep.navigateTo(target: RoomPosition, travelToOptions: TravelToOptions? = null) = Traveler.travelTo(this, target, travelToOptions)

}
